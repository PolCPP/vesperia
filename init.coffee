###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

net = require 'net'
mysql = require 'mysql'
settings = require './settings'
vespServer = require './server'
tethClient = require './client'

console.log ""
console.log "############################################################"
console.log "# Vesperia: A tethealla management port proxy and api server"
console.log "#"
console.log "# Copyright 2012 Pol Cámara"
console.log "# Released under the MIT License"
console.log "# Check LICENSE.MIT for more details"
console.log "############################################################"

connection = mysql.createConnection settings.dbConnection

client = new tethClient net, settings.tetheallaPort, settings.tetheallaMaxPort, settings.tetheallaHost
client.initialize()

server = new vespServer net, settings.vesperiaPort, settings.vesperiaHost, connection, client
server.initialize()