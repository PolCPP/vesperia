###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

mysql = require 'mysql'
settings = require './settings'
Security = require "./model/security"
wrongArgs = true
command = ""
value = ""
wrongMessage = "Wrong arguments set, it must be:\n manage_key add secretkey \n manage_key remove secretkey"
connection = mysql.createConnection settings.dbConnection


if process.argv.length == 3 && process.argv[0] != "coffee"
  wrongArgs = false
  command = process.argv[1]
  value = process.arv[2]

if process.argv.length == 4 && process.argv[0] == "coffee" 
  wrongArgs = false
  command = process.argv[2]
  value = process.argv[3]

connection.connect (err) ->
  if not err
    if wrongArgs
      console.log wrongMessage
    else
      security = new Security connection
      if command == "add"
        security.addKey value, (dbKey) -> console.log "Key added successfully ID: " + dbKey  
      else if command == "remove"
        security.delKey value, (rows) -> 
          if rows > 0
             console.log "Key removed successfully"
          else 
             console.log "Key wasn't found"
      else if command = "search"
        security.valKey value, false, (result) -> 
          if result
            console.log "Key found"
          else
            console.log "Key not found"
      else 
        console.log wrongMessage
      connection.end()
  else
    console.log err
