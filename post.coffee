querystring = require 'querystring'
http = require 'http'

post_data = querystring.stringify
  'username' : 'White Ninja'
  'message' : 'Test message'
  'gc' : '42000001'
  'lobby': '1'
  'secret': 'crab bongos'
  'submit': 'true'
  'json': 'true'

post_options = 
      host: 'edenserv.net'
      port: '80'
      path: '/chatgate/gateway.php'
      method: 'POST'
      headers: 
          'Content-Type': 'application/x-www-form-urlencoded'
          'Content-Length': post_data.length


post_req = http.request post_options, (res) -> 
  res.setEncoding('utf8')
  res.on 'data', (chunk) -> 
    console.log('Response: ' + chunk)

post_req.write(post_data)
post_req.end()
