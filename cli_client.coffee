###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.

 Vesperia Command line client:
 It has 2 modes. Direct connection and proxied. 
 Default mode is proxied, you can change it via parameters
###

crypto = require "crypto"
net = require 'net'

# Configuration

# The host and port from the tethealla server or proxy server
host = "127.0.0.1"
port = "9825"

#Api hash is required for proxied connection auth, remember to set one that's on the proxy db 
api_hash = "2925b6b28d87e54fe773586b7b6025f6"

# UID is and unique id since we'll be emulating the proxy. It uses the same method as the server
# to generate a unique md5 string
uid =  crypto.createHash("md5").update((new Date().getTime()+Math.floor(Math.random()*1000)).toString()).digest("hex")

# End of configuration.
ConnectionModes = 
  direct: 1
  proxied: 2

mode = ConnectionModes.direct
command = ""
pass = ""


connection = net.createConnection port, host

connection.on 'connect',  ->
  console.log "Opened connection to #{host}:#{port}"
  connection.write "110test"

connection.on 'data', (data) ->
  if data.toString() == "102" 
    connection.write "210happyhour" 
    console.log "Received: #{data}"

