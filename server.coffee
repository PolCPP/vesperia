###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

Security = require "./model/security"
crypto = require "crypto"
querystring = require 'querystring'
http = require 'http'
settings = require './settings'
ent = require 'ent'
protocol = require './protocol'
xmpp = require 'simple-xmpp'
LobbyUser = require './lobby_user'

#The global var is used to jump from a socket to the server.
gServer = false
chatboxQueue = []
lobbyUsers = {}

class Server
  chatbox_req = false
  clients:  {}
  socket: false
  security: false

  constructor: (@net, @port, @host, @connection, @client) ->
    gServer = this
    this.security = new Security connection
    this.client.events.on 'message', (message) -> gServer._parseTethPacket(message)
    setInterval gServer.sendQueue, 300
      
  ###
  Initializes the server
  Example usage:
    serverInstance.initialize()
  ###
  initialize: ->
    server = this.net.createServer this._startSocket
    server.listen this.port, this.host

  sendQueue: ->
    if gServer.client.connected
      message = chatboxQueue.shift()
      gServer.client.stream.write message if typeof message != "undefined"

  _startSocket: (socket) -> 
    socket.authenticated = false
    socket.uid = false
    socket.write protocol.Server.Auth.loginRequired
    socket.on 'data', (data) -> gServer._onData this,data.toString().replace("\n","")

  _onData: (socket, data) ->
    console.log "Recieved: " +data
    this._parsePacket socket, data if this._authenticate socket, data

  _authenticate: (socket, data) ->
    hashed = false
    return true if socket.authenticated == true
    if data.length > 3
      command = data.substring 0, 3
      if command == protocol.Client.Auth.loginUnsafe || command == protocol.Client.Auth.loginSafe
        hashed = true if command == protocol.Client.Auth.loginSafe
        password = data.substring 3, data.length
        password = password.trim()
        this.security.valKey password, hashed, (keyExists) ->
          if keyExists
            socket.authenticated = true
            socket.uid =  crypto.createHash("md5").update((new Date().getTime()+Math.floor(Math.random()*1000)).toString()).digest("hex")
            socket.write protocol.Server.Auth.success
            gServer.clients[socket.uid] = socket
          else
            gServer._disconnectClient socket, protocol.Server.Auth.unauthorized
      else
        gServer._disconnectClient socket, protocol.Server.Auth.unauthorized
    return false
 
  _disconnectClient: (socket,code) ->
    delete gServer.clients[socket.uid] if socket.uid
    #socket.write(code)
    #socket.end()

  _parsePacket: (socket,data) ->
    command = data.substring 0, 3
    details = data.substring 3, data.length
    details = details.trim()
    switch command
      when protocol.Client.Proxy.sendToServer then gServer._sendToTethealla(socket,details)
      when protocol.Client.Proxy.chatToServer
        details = data.substring 3, 94
        chatboxQueue.push(protocol.Server.Proxy.tethReceiveChat+details)

  _parseTethPacket: (data) ->
    if data.length >= 3
      command = data[0..2].toString()
      switch command
        when protocol.Tethealla.Chat.chatFromPso
          lobby = data[3..4].toString('utf8').trim()
          gc = data[5..12].toString('utf8').trim()
          char= data[13..32].toString('utf16le').trim().replace("\tE","").replace(/\0/g,"")
          message = data[33..].toString('utf16le').trim().replace(/\0/g,"")
          message = message.substr(1) if message.substr(0,1) == "/"
          if gc != settings.botGuildcard
            console.log "225 packet:" + data.toString() + " hex:"+ data.toString("hex")
            console.log "Message received: Sending it to the chatbox"
            this._sendToChatbox char, message, gc, lobby
        when  protocol.Tethealla.Proxy.commandSuccess , protocol.Tethealla.Proxy.commandSuccessReply, protocol.Tethealla.Proxy.commandFailed
          dataString = data.toString();
          if dataString.length > 35
            uid = dataString.substring 3, 35
            message = dataString.substring 35, dataString.length
            if typeof gServer.clients[uid] != "undefined"
              gServer.clients[uid].write message

  _sendToTethealla: (socket,action) ->    
    if gServer.client
      if gServer.client.connected
        gServer.client.stream.write protocol.Server.Proxy.tethExecuteCommand+socket.uid+action
      else
        gServer.client.reconnect()        
        socket.write protocol.Server.Proxy.clientTetheallaOffline

  _sendToChatbox: (char, message, gc, lobby) ->
    this._sendToXMPP(char, message, gc)
    postData = querystring.stringify
      'username' : ent.encode char
      'message' : ent.encode message
      'gc' : ent.encode gc
      'lobby': ent.encode lobby
      'secret': settings.chatboxSecret
      'submit': 'true'
      'json': 'true'
    postOptions =
      host: settings.chatboxHost
      port: settings.chatboxPort
      path: settings.chatboxPath
      method: 'POST'
      headers:
        'Content-Type': 'application/x-www-form-urlencoded'
        'Content-Length': postData.length
    chatboxReq = http.request postOptions, (res) ->
      res.setEncoding('utf8')
      res.on 'data', (chunk) ->
        console.log('Response: ' + chunk)
    chatboxReq.write(postData)
    chatboxReq.end()

  _sendToXMPP: (char,message,gc) ->
    if typeof lobbyUsers[char+"-"+gc] == "undefined"
      lobbyUsers[char+"-"+gc] = new LobbyUser char+"-"+gc, xmpp.constructor
    lobbyUsers[char+"-"+gc].say message
    # Since for now we don't have a way to know who's on the lobby: we do it like on the old chatbox, when someone
    # talks it gets add to the roster, and if he doesn't say anything on X time (on settings) he gets removed.
    for user of lobbyUsers
      delete lobbyUsers[user] if lobbyUsers[user].lastUpdate + settings.xmppTimeout < new Date().getTime()

module.exports = Server