###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

# Public port and ip from the Tethealla server to connect
vesperiaHost = "127.0.0.1"
vesperiaPort = 9825

# Public port and ip from the Tethealla server to connect
tetheallaHost = "127.0.0.1"
tetheallaPort = 8825
tetheallaMaxPort = 8830

#Database conection object
dbConnection = 
  host     : '127.0.0.1'
  port     : '3306'
  user     : 'root'
  password : 'root'
  database : 'psotest'

# Max tethealla connection attempts (client)
maxAttempts = 3

# Max tethealla reconnection wait time
attemptWait = 2000

#Asks the server for the messsages said on the lobby
askChat = true

#Bot Guildcard, so we ignore their messages to avoid an infinite loop.
botGuildcard = "42000000"

#Put a number between the "" if you only want messages from a determinate lobby
chatLobby = "01"

#Chatbox settings: The server sends a post request to the web server to publish chatbox message
chatboxHost = "edenserv.net"
chatboxPort = "80"
chatboxPath = "/chatgate/gateway.php"
chatboxSecret = "3a89889bb937c6544592ad60f87f948d"

#XMPP Bot Info data
xmppJID = 'bot@edenserv'
xmppPassword = 'password'
xmppHost = 'edenserv.net'
xmppPort = 5222
xmppLobbyConf = 'lobby@conference.edenserv'
# Set to 5 min without saying anything in the lobby. Please note this is a temporal setup until 
#there's an update on the ship that sends a list of online users.
xmppTimeout = 300000 

module.exports.vesperiaHost = vesperiaHost
module.exports.vesperiaPort = vesperiaPort
module.exports.tetheallaHost = tetheallaHost      
module.exports.tetheallaMaxPort = tetheallaMaxPort      
module.exports.tetheallaPort = tetheallaPort
module.exports.dbConnection = dbConnection
module.exports.maxAttempts = maxAttempts
module.exports.attemptWait = attemptWait
module.exports.askChat = askChat
module.exports.chatLobby = chatLobby
module.exports.botGuildcard = botGuildcard
module.exports.chatboxHost = chatboxHost
module.exports.chatboxPort = chatboxPort
module.exports.chatboxPath = chatboxPath
module.exports.chatboxSecret = chatboxSecret
module.exports.xmppJID = xmppJID
module.exports.xmppPassword = xmppPassword
module.exports.xmppHost = xmppHost
module.exports.xmppPort = xmppPort
module.exports.xmppLobbyConf = xmppLobbyConf