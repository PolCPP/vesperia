settings = require './settings'

class LobbyUser  
  constructor: (@name, xmppConstructor) ->
    this.lastUpdate = new Date().getTime()
    this.xmpp = new xmppConstructor()
    this.messageQueue = []
    this.online = false
    that = this
    this.xmpp.on 'online', () -> 
      that.online = true;
      that.xmpp.join settings.xmppLobbyConf+"/"+that.name
      that._sendMessages()

    this.xmpp.connect {
      jid: settings.xmppJID
      password: settings.xmppPassword
      host: settings.xmppHost
      port: settings.xmppPort
    }

  say: (message) ->
    this.lastUpdate = new Date().getTime();
    this.messageQueue.push message
    this._sendMessages() if this.online 

  _sendMessages: ->
    this.xmpp.send settings.xmppLobbyConf, item, true while item = this.messageQueue.shift()

module.exports = LobbyUser