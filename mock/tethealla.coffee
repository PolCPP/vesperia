###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

# This is a little mock server that/will imitate tethealla mana port
net = require 'net'

domain = 'localhost'
port = 8825

server = net.createServer (socket) ->
  console.log "#{socket.remoteAddress} connected"

  socket.on 'data', (data) ->
    console.log "Recieved: " + data
    data = data.toString()
    if data == "205" || data == "20501"
      buf = new Buffer [0x32, 0x32, 0x35, 0x31, 0x00, 0x34, 0x32, 0x30, 0x30, 0x30, 0x30, 0x30, 0x34, 0x09, 0x00, 0x45, 0x00, 0x42, 0x00, 0x6c, 0x00, 0x61, 0x00, 0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x00, 0x65, 0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x21, 0x00, 0x00, 0x00, 0x06]
      socket.write buf
    if data.length > 35
      command = data.substring 0, 3
      uid = data.substring 3, 35
      action = data.substring 35, data.length
      if command == "200"
        socket.write "220" + uid + "Command ran successfully" 

console.log "Listening to #{domain}:#{port}"
server.listen port, domain
