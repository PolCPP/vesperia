CREATE TABLE  `psotest`.`api_keys` (
`id` INT NOT NULL AUTO_INCREMENT ,
`value` VARCHAR( 32 ) NOT NULL ,
PRIMARY KEY (  `id` ) ,
UNIQUE (
`value`
)
) ENGINE = INNODB;
