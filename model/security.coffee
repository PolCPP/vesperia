###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

crypto = require 'crypto'

gConnection = null

class Security
  constructor: (@connection) ->
    gConnection = connection    
  ###
  (Private) Generates the password hash
  Example usage:
    hash = this.makeHash key
  ###
  _makeHash: (key) -> crypto.createHash("md5").update(key).digest("hex")

  ###
  Adds a new security (api) key to the database 
  Example usage: 
    securityInstance.addKey "MyKey", (dbKey) -> console.log dbKey
  ###
  addKey: (secretKey, cb) ->
    hash = this._makeHash secretKey
    gConnection.query "INSERT INTO api_keys (id, value) values (null,?)", [hash], (err, results) -> 
      cb results.insertId if results

  ###
  Validates if the security (api) key is correct. Hashed means that the password is hashed
  Example usage:
    security_Instance.valKey "MyKey", false, (keyExists) -> console.log "Logged in" if keyExists
  ###
  valKey: (secretKey,hashed,cb) ->
    hash = this._makeHash secretKey if !hashed
    gConnection.query "SELECT * FROM api_keys where value = ?", [hash], (err, results) ->
      cb results.length != 0

  ###
  Deletes a security (api) key by secret.
  Example usage:
    security_instance.delKey "MyKey", (key, err) -> console.log "Deleted" if not err
  ###
  delKey: (secretKey, cb) ->
    hash = this._makeHash secretKey
    gConnection.query "DELETE FROM api_keys where value = ?", [hash], (err, results) ->
      cb results.affectedRows

module.exports = Security
