###
 Vesperia: A Tethealla management port proxy and api server

 Copyright 2012 Pol Cámara
 Released under the MIT license
 Check LICENSE.MIT for more details.
###

crypto = require 'crypto'

gConnection = null

protocol = {
  Client: {
    Auth: {
      loginUnsafe: "110"
      loginSafe: "111"
    }

    Proxy: {
      sendToServer: "210"
      chatToServer: "216"
    }
  }

  Server: {
    Auth: {
      loginRequired: "100"
      unauthorized: "101"
      success: "102"
    }

    Proxy: {
      tethExecuteCommand: "200"
      clientResponseFromTeth: "201"
      clientTetheallaOffline: "202"
      tethReceiveChat: "206"
    }

    Chat: {
      requestLobbies: "205"
      requestOneLobby: "205"
    }
  }

  Tethealla: {
    Proxy: {
      commandSuccess: "220"
      commandSuccessReply: "220"
      commandFailed: "221"
    }

    Chat: {
      chatFromPso: "225"
    }
  }
}

module.exports = protocol
